package com.tianyalei.core.zuulauth.config;

import com.tianyalei.core.zuulauth.config.properties.AuthFetchDurationProperties;
import com.tianyalei.core.zuulauth.zuul.AuthInfoHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.annotation.Resource;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * @author wuweifeng wrote on 2019-08-13.
 */
@EnableConfigurationProperties(AuthFetchDurationProperties.class)
public class AuthConfigure {
    public Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private AuthFetchDurationProperties properties;

    @Bean
    @ConditionalOnMissingBean
    AuthInfoHolder authInfoHolder(){
        logger.info(" AuthInfoHolder 初始化 ");
        return new AuthInfoHolder(stringRedisTemplate);
    }

    @Resource
    AuthInfoHolder authInfoHolder;


    /**
     * 启动后自动获取redis里的权限信息到内存
     */
    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        logger.info("开始拉取所有客户端mapping信息");
        //拉取mapping信息
        authInfoHolder.saveAllMappingInfo();

        logger.info(authInfoHolder.keys().toString());
        logger.info("拉取完毕");

        ScheduledExecutorService scheduledExecutor = Executors.newScheduledThreadPool(3);
        if (properties.getMappingFetch() > 0) {
            scheduledExecutor.scheduleAtFixedRate(authInfoHolder::saveAllMappingInfo,
                    properties.getDelay(), properties.getMappingFetch(), properties.getTimeUnit());
        }
        if (properties.getRoleFetch() > 0) {
            scheduledExecutor.scheduleAtFixedRate(authInfoHolder::saveAllUserRole,
                    properties.getDelay(), properties.getRoleFetch(), properties.getTimeUnit());
        }
        if (properties.getCodeFetch() > 0) {
            scheduledExecutor.scheduleAtFixedRate(authInfoHolder::saveAllRoleCode,
                    properties.getDelay(), properties.getCodeFetch(), properties.getTimeUnit());
        }

    }

}
